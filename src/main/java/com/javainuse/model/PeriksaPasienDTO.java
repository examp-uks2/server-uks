package com.javainuse.model;

import java.util.Date;

public class PeriksaPasienDTO {

    private long id;

    private String nama_pasien;
    private String status_pasien;
    private String jabatan;
    private Date tanggal;
    private String keterangan;
    private String status;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNama_pasien() {
        return nama_pasien;
    }

    public void setNama_pasien(String nama_pasien) {
        this.nama_pasien = nama_pasien;
    }

    public String getStatus_pasien() {
        return status_pasien;
    }

    public void setStatus_pasien(String status_pasien) {
        this.status_pasien = status_pasien;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
