package com.javainuse.model;

import javax.persistence.*;

@Entity
@Table(name = "diagnosa")
public class DAODiagnosa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private long id;

    private String nama_diagnosa;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNama_diagnosa() {
        return nama_diagnosa;
    }

    public void setNama_diagnosa(String nama_diagnosa) {
        this.nama_diagnosa = nama_diagnosa;
    }
}
