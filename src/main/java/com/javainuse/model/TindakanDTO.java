package com.javainuse.model;

public class TindakanDTO {

    private long id;

    private String nama_tindakan;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNama_tindakan() {
        return nama_tindakan;
    }
    public void setNama_tindakan(String nama_tindakan) {
        this.nama_tindakan = nama_tindakan;
    }
}
