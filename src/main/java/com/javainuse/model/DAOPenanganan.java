package com.javainuse.model;

import javax.persistence.*;

@Entity
@Table(name = "penanganan")
public class DAOPenanganan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private long id;

    private String nama_penanganan;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNama_penanganan() {
        return nama_penanganan;
    }

    public void setNama_penanganan(String nama_penanganan) {
        this.nama_penanganan = nama_penanganan;
    }
}
