package com.javainuse.controller;

import com.javainuse.model.DAOGuru;
import com.javainuse.model.DAOPeriksaPasien;
import com.javainuse.model.GuruDTO;
import com.javainuse.model.PeriksaPasienDTO;
import com.javainuse.service.JwtGuruDetailService;
import com.javainuse.service.JwtPeriksaPasienDetailService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/pasien/periksaPasien")


public class PeriksaPasienController {

    public static final Logger logger = LoggerFactory.getLogger(PeriksaPasienController.class);

    @Autowired
    private JwtPeriksaPasienDetailService productDetailService;


    // -------------------Create a Product-------------------------------------------

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> createProduct(@RequestBody PeriksaPasienDTO product) throws SQLException, ClassNotFoundException {
        logger.info("Creating Product : {}", product);

        productDetailService.save(product);

        return new ResponseEntity<>(product, HttpStatus.CREATED);
    }


    // -------------------Retrieve All Products--------------------------------------------

    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<DAOPeriksaPasien>> listAllProducts()  {

        List<DAOPeriksaPasien> products = productDetailService.findAll();

        return new ResponseEntity<>(products, HttpStatus.OK);
    }


    // -------------------Retrieve Single Product By Id------------------------------------------

    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getProduct(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching Product with id {}", id);

        Optional<DAOPeriksaPasien> product = productDetailService.findById(id);

        if (product == null) {
            logger.error("Product with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Product with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(product, HttpStatus.OK);
    }


    // ------------------- Update a Product ------------------------------------------------

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateProduct(@PathVariable("id") long id, @RequestBody DAOPeriksaPasien product) throws SQLException, ClassNotFoundException {
        logger.info("Updating Product with id {}", id);

        Optional<DAOPeriksaPasien> currentProduct = productDetailService.findById(id);

        if (currentProduct == null) {
            logger.error("Unable to update. Product with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to upate. Product with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        currentProduct.orElseThrow().setNama_pasien(product.getNama_pasien());
        currentProduct.orElseThrow().setStatus_pasien(product.getStatus_pasien());
        currentProduct.orElseThrow().setJabatan(product.getJabatan());
        currentProduct.orElseThrow().setTanggal(product.getTanggal());
        currentProduct.orElseThrow().setKeterangan(product.getKeterangan());
        currentProduct.orElseThrow().setStatus(product.getStatus());

        productDetailService.update(currentProduct.get().getId());
        return new ResponseEntity<>(currentProduct, HttpStatus.OK);
    }


    // ------------------- Delete a Product-----------------------------------------

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProduct(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting Book with id {}", id);

        productDetailService.delete(id);
        return new ResponseEntity<DAOGuru>(HttpStatus.NO_CONTENT);
    }

}