package com.javainuse.controller;

import com.javainuse.model.DAOGuru;
import com.javainuse.model.DAOSiswa;
import com.javainuse.model.GuruDTO;
import com.javainuse.model.SiswaDTO;
import com.javainuse.service.JwtGuruDetailService;
import com.javainuse.service.JwtSiswaDetailService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/pasien")
public class SiswaController {

    public static final Logger logger = LoggerFactory.getLogger(SiswaController.class);

    @Autowired
    private JwtSiswaDetailService productDetailService;


    // -------------------Create a Product-------------------------------------------

    @RequestMapping(value = "/siswa/add", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> createProduct(@RequestBody SiswaDTO product) throws SQLException, ClassNotFoundException {
        logger.info("Creating Product : {}", product);

        productDetailService.save(product);

        return new ResponseEntity<>(product, HttpStatus.CREATED);
    }


    // -------------------Retrieve All Products--------------------------------------------

    @RequestMapping(value = "/siswa/all", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<DAOSiswa>> listAllProducts() throws SQLException, ClassNotFoundException {

        List<DAOSiswa> products = productDetailService.findAll();

        return new ResponseEntity<>(products, HttpStatus.OK);
    }


    // -------------------Retrieve Single Product By Id------------------------------------------

    @RequestMapping(value = "/siswa/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getProduct(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching Product with id {}", id);

        Optional<DAOSiswa> product = productDetailService.findById(id);

        if (product == null) {
            logger.error("Product with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Product with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(product, HttpStatus.OK);
    }


    // ------------------- Update a Product ------------------------------------------------

    @RequestMapping(value = "/siswa/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateProduct(@PathVariable("id") long id, @RequestBody DAOSiswa product) throws SQLException, ClassNotFoundException {
        logger.info("Updating Product with id {}", id);

        Optional<DAOSiswa> currentProduct = productDetailService.findById(id);

        if (currentProduct == null) {
            logger.error("Unable to update. Product with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to upate. Product with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        currentProduct.orElseThrow().setNama_siswa(product.getNama_siswa());
        currentProduct.orElseThrow().setKelas(product.getKelas());
        currentProduct.orElseThrow().setTempat_tanggal_lahir(product.getTempat_tanggal_lahir());
        currentProduct.orElseThrow().setAlamat(product.getAlamat());

        productDetailService.update(currentProduct.get().getId());
        return new ResponseEntity<>(currentProduct, HttpStatus.OK);
    }


    // ------------------- Delete a Product-----------------------------------------

    @RequestMapping(value = "/siswa/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProduct(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting Book with id {}", id);

        productDetailService.delete(id);
        return new ResponseEntity<DAOGuru>(HttpStatus.NO_CONTENT);
    }

}