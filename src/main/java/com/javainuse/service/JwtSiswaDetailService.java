package com.javainuse.service;

import com.javainuse.dao.SiswaDao;
import com.javainuse.model.DAOSiswa;
import com.javainuse.model.SiswaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtSiswaDetailService {

    @Autowired
    private SiswaDao productDao;
    private long id;

    public JwtSiswaDetailService() {
    }

    public DAOSiswa save(SiswaDTO product) {

        DAOSiswa newProduct = new DAOSiswa();
        newProduct.setNama_siswa(product.getNama_siswa());
        newProduct.setKelas(product.getKelas());
        newProduct.setTempat_tanggal_lahir(product.getTempat_tanggal_lahir());
        newProduct.setAlamat(product.getAlamat());

        return productDao.save(newProduct);
    }


    public Optional<DAOSiswa> findById(Long id) {
        return Optional.ofNullable(productDao.findById(id));
    }


    public List<DAOSiswa> findAll() {
        List<DAOSiswa> products = new ArrayList<>();
        productDao.findAll().forEach(products::add);
        return products;
    }


    public void delete(Long id) {
        DAOSiswa product = productDao.findById(id);
        productDao.delete(product);
    }


    public DAOSiswa update(Long id) {
        DAOSiswa product = productDao.findById(id);
        return productDao.save(product);
    }

}