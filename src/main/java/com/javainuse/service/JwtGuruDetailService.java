package com.javainuse.service;

import com.javainuse.dao.GuruDao;
import com.javainuse.model.DAOGuru;
import com.javainuse.model.GuruDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtGuruDetailService {

    @Autowired
    private GuruDao productDao;
    private long id;

    public JwtGuruDetailService() {
    }

    public DAOGuru save(GuruDTO product) {

        DAOGuru newProduct = new DAOGuru();
        newProduct.setNama_guru(product.getNama_guru());
        newProduct.setTempat_tanggal_lahir(product.getTempat_tanggal_lahir());
        newProduct.setAlamat(product.getAlamat());

        return productDao.save(newProduct);
    }


    public Optional<DAOGuru> findById(Long id) {
        return Optional.ofNullable(productDao.findById(id));
    }


    public List<DAOGuru> findAll() {
        List<DAOGuru> products = new ArrayList<>();
        productDao.findAll().forEach(products::add);
        return products;
    }


    public void delete(Long id) {
        DAOGuru product = productDao.findById(id);
        productDao.delete(product);
    }


    public DAOGuru update(Long id) {
        DAOGuru product = productDao.findById(id);
        return productDao.save(product);
    }

}