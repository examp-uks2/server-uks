package com.javainuse.service;

import com.javainuse.dao.GuruDao;
import com.javainuse.dao.PeriksaPasienDao;
import com.javainuse.model.DAOGuru;
import com.javainuse.model.DAOPeriksaPasien;
import com.javainuse.model.GuruDTO;
import com.javainuse.model.PeriksaPasienDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtPeriksaPasienDetailService {

    @Autowired
    private PeriksaPasienDao productDao;
    private long id;

    public JwtPeriksaPasienDetailService() {
    }

    public DAOPeriksaPasien save(PeriksaPasienDTO product) {

        DAOPeriksaPasien newProduct = new DAOPeriksaPasien();
        newProduct.setNama_pasien(product.getNama_pasien());
        newProduct.setStatus_pasien(product.getStatus_pasien());
        newProduct.setJabatan(product.getJabatan());
        newProduct.setJabatan(product.getJabatan());
        newProduct.setKeterangan(product.getKeterangan());
        newProduct.setStatus(product.getStatus());


        return productDao.save(newProduct);
    }


    public Optional<DAOPeriksaPasien> findById(Long id) {
        return Optional.ofNullable(productDao.findById(id));
    }


    public List<DAOPeriksaPasien> findAll() {
        List<DAOPeriksaPasien> products = new ArrayList<>();
        productDao.findAll().forEach(products::add);
        return products;
    }


    public void delete(Long id) {
        DAOPeriksaPasien product = productDao.findById(id);
        productDao.delete(product);
    }


    public DAOPeriksaPasien update(Long id) {
        DAOPeriksaPasien product = productDao.findById(id);
        return productDao.save(product);
    }

}