package com.javainuse.service;


import com.javainuse.dao.PenangananDao;
import com.javainuse.model.DAOPenanganan;
import com.javainuse.model.PenangananDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtPenangananDetailService {

    @Autowired
    private PenangananDao productDao;
    private long id;

    public JwtPenangananDetailService() {
    }

    public DAOPenanganan save(PenangananDTO product) {

        DAOPenanganan newProduct = new DAOPenanganan();

        return productDao.save(newProduct);
    }


    public Optional<DAOPenanganan> findById(Long id) {
        return Optional.ofNullable(productDao.findById(id));
    }


    public List<DAOPenanganan> findAll() {
        List<DAOPenanganan> products = new ArrayList<>();
        productDao.findAll().forEach(products::add);
        return products;
    }


    public void delete(Long id) {
        DAOPenanganan product = productDao.findById(id);
        productDao.delete(product);
    }


    public DAOPenanganan update(Long id) {
        DAOPenanganan product = productDao.findById(id);
        return productDao.save(product);
    }

}