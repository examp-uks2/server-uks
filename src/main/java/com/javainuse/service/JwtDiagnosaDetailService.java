package com.javainuse.service;

import com.javainuse.dao.DiagnosaDao;
import com.javainuse.model.DAODiagnosa;
import com.javainuse.model.DAOGuru;
import com.javainuse.model.DiagnosaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtDiagnosaDetailService {

    @Autowired
    private DiagnosaDao productDao;
    private long id;

    public JwtDiagnosaDetailService() {
    }

    public DAODiagnosa save(DiagnosaDTO product) {

        DAODiagnosa newProduct = new DAODiagnosa();
        newProduct.setNama_diagnosa(product.getNama_diagnosa());

        return productDao.save(newProduct);
    }


    public Optional<DAODiagnosa> findById(Long id) {
        return Optional.ofNullable(productDao.findById(id));
    }


    public List<DAODiagnosa> findAll() {
        List<DAODiagnosa> products = new ArrayList<>();
        productDao.findAll().forEach(products::add);
        return products;
    }


    public void delete(Long id) {
        DAODiagnosa product = productDao.findById(id);
        productDao.delete(product);
    }


    public DAODiagnosa update(Long id) {
        DAODiagnosa product = productDao.findById(id);
        return productDao.save(product);
    }

}