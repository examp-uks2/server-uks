package com.javainuse.service;

import com.javainuse.dao.ObatDao;
import com.javainuse.model.DAOObat;
import com.javainuse.model.ObatDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtObatDetailService {

    @Autowired
    private ObatDao productDao;
    private long id;

    public JwtObatDetailService() {
    }

    public DAOObat save(ObatDTO product) {

        DAOObat newProduct = new DAOObat();

        return productDao.save(newProduct);
    }


    public Optional<DAOObat> findById(Long id) {
        return Optional.ofNullable(productDao.findById(id));
    }


    public List<DAOObat> findAll() {
        List<DAOObat> products = new ArrayList<>();
        productDao.findAll().forEach(products::add);
        return products;
    }


    public void delete(Long id) {
        DAOObat product = productDao.findById(id);
        productDao.delete(product);
    }


    public DAOObat update(Long id) {
        DAOObat product = productDao.findById(id);
        return productDao.save(product);
    }

}