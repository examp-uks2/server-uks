package com.javainuse.service;

import com.javainuse.dao.GuruDao;
import com.javainuse.dao.KaryawanDao;
import com.javainuse.model.DAOGuru;
import com.javainuse.model.DAOKaryawan;
import com.javainuse.model.KaryawanDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtKaryawanDetailService {

    @Autowired
    private KaryawanDao productDao;
    private long id;

    public JwtKaryawanDetailService() {
    }

    public DAOKaryawan save(KaryawanDTO product) {

        DAOKaryawan newProduct = new DAOKaryawan();
        newProduct.setNama_karyawan(product.getNama_karyawan());
        newProduct.setTempat_tanggal_lahir(product.getTempat_tanggal_lahir());
        newProduct.setAlamat(product.getAlamat());

        return productDao.save(newProduct);
    }


    public Optional<DAOKaryawan> findById(Long id) {
        return Optional.ofNullable(productDao.findById(id));
    }


    public List<DAOKaryawan> findAll() {
        List<DAOKaryawan> products = new ArrayList<>();
        productDao.findAll().forEach(products::add);
        return products;
    }


    public void delete(Long id) {
        DAOKaryawan product = productDao.findById(id);
        productDao.delete(product);
    }


    public DAOKaryawan update(Long id) {
        DAOKaryawan product = productDao.findById(id);
        return productDao.save(product);
    }

}