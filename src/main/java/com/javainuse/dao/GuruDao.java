package com.javainuse.dao;

import com.javainuse.model.DAOGuru;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository

public interface GuruDao extends CrudRepository<DAOGuru, Integer> {
    DAOGuru findById(long id);
}
