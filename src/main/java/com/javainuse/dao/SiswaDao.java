package com.javainuse.dao;

import com.javainuse.model.DAOSiswa;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository

public interface SiswaDao extends CrudRepository<DAOSiswa, Integer> {
    DAOSiswa findById(long id);
}
