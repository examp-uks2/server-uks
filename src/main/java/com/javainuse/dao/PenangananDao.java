package com.javainuse.dao;

import com.javainuse.model.DAOPenanganan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository

public interface PenangananDao extends CrudRepository<DAOPenanganan, Integer> {
    DAOPenanganan findById(long id);
}
