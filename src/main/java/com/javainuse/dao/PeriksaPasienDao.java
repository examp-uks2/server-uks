package com.javainuse.dao;

import com.javainuse.model.DAOPeriksaPasien;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository

public interface PeriksaPasienDao extends CrudRepository<DAOPeriksaPasien, Integer> {
    DAOPeriksaPasien findById(long id);
}
